/*!
\file jeu.h
\autor Jalbert Sylvain
\version 1
\date 14 novembre 2019
\brief le fichier d'entête du fichier jeu.c qui contient les déclaration des fonctions relative au comportement du jeu
*/

#ifndef __JEU_H_
#define __JEU_H_

//on utilise la librairie utile pour executer des comandes systèmes dans le terminal
#include <stdlib.h>

#include "aGagner.h"
#include "buffer.h"

/*!
\fn void init ( int ttint_plateau[N][N] )
\author Jalbert Sylvain
\version 0.1 Premier jet
\date 8 novembre 2019
\brief une procedure qui initialise le tableau de tableau en mettant des -1 dans ttes les cases
\param ttint_plateau[N][N] tableau de tableau d'entier, représentant la grille de jeu
*/
void init(int ttint_plateau[N][N]);

/*!
\fn void affichage ( int ttint_plateau[N][N] )
\author Jalbert Sylvain
\version 0.1 Premier jet
\date 8 novembre 2019
\brief une procedure qui affiche la grille passé en paramettre
\param ttint_plateau[N][N] tableau de tableau d'entier, représentant la grille de jeu
*/
void affichage(int ttint_plateau[N][N]);

/*!
\fn int jouer ( int ttint_plateau[N][N], int int_joueur, int int_x )
\author Jalbert Sylvain
\version 0.1 Premier jet
\date 8 novembre 2019
\brief une fonction qui va permetre à un joueur de jouer
\param ttint_plateau[N][N] tableau de tableau d'entier, représentant la grille de jeu
\param int_joueur le numero de joueur qui joue
\param int_x colonne que le joueur joue
\return 1 si tout c'est bien passé, 0 sinon
*/
int jouer(int ttint_plateau[N][N], int int_joueur, int int_x);

/*!
\fn void tourDeJeu ( int ttint_plateau[N][N] )
\author Jalbert Sylvain
\version 0.1 Premier jet
\date 11 novembre 2019
\brief une procedure qui va faire jouer les joueurs chaqu'un leurs tours jusqu'a la fin de la partie et afficher le gagnant
\param ttint_plateau[N][N] tableau de tableau d'entier, représentant la grille de jeu
*/
void tourDeJeu(int ttint_plateau[N][N]);

#endif
