/*!
\file aGagner.c
\autor Jalbert Sylvain
\version 1
\date 14 novembre 2019
\brief le fichier qui contient toutes les fonction relatif au test: si il y a un gagnant.
*/

#include "aGagner.h"

/*!
\fn int aGagnerHorizontale ( int ttint_plateau[N][N] )
\author Jalbert Sylvain
\version 0.1 Premier jet
\date 8 novembre 2019
\brief une fonction qui va verifier si 4 pions sont aligné horizontalement
\param ttint_plateau[N][N] tableau de tableau d'entier, représentant la grille de jeu
\return -1 si personne n'a gagner, sinon, renvoie le numero du joueur gagnant
*/
int aGagnerHorizontale(int ttint_plateau[N][N]){
  //DECLARATION DES VARIABLES
  int int_i; //variables qui va parcourir les indice des lignes du tableau
  int int_j; //variables qui va parcourir les indice des colonnes du tableau
  int int_nbr; //le nombre de pion aligné
  int int_tmp; //la valeur dans la case précédente
  int int_valeurReturn; //la valeur à retourner

  //INITIALISATION DES VARIABLES
  //on considère au départ que personne n'a gagner
  int_valeurReturn = -1;

  //parcourir toutes les lignes
  for(int_i = 0 ; int_i < N ; int_i++){
    //(ré)initialisation du nombre de pion aligné
    int_nbr = 1;
    //(ré)initialisation de la case précédente
    int_tmp = -1;
    //parcourir toutes les colonnes
    for(int_j = 0 ; int_j < N ; int_j++){
      //si la case courante à été joué
      if(ttint_plateau[int_i][int_j]!=-1){
        //si un pion est égal au précédent
        if(ttint_plateau[int_i][int_j]==int_tmp){
          //incrémenter le nombre de pion aligné
          int_nbr++;
          //si 4 pions sont alligné, on retourne alors le numéro du joueur gagnant
          if(int_nbr == 4) int_valeurReturn = ttint_plateau[int_i][int_j];
        }
        //sinon = si la case courante n''est pas égale à la précedente'
        else{
          //on réinitialise le nombre de pion aligné
          int_nbr = 1;
        }
      }
      //sinon = si la case courante n'a pas été joué
      else{
          //on réinitialise le nombre de pion aligné
          int_nbr = 1;
      }
      //mettre à jour la case précédente
      int_tmp=ttint_plateau[int_i][int_j];
    }
  }
  //si le programmearrive jusque là, alors personne n'a gagné, on renvoie alors -1
  return(int_valeurReturn);
}

/*!
\fn int aGagnerVerticale ( int ttint_plateau[N][N] )
\author Jalbert Sylvain
\version 0.1 Premier jet
\date 8 novembre 2019
\brief une fonction qui va verifier si 4 pions sont aligné verticalement
\param ttint_plateau[N][N] tableau de tableau d'entier, représentant la grille de jeu
\return -1 si personne n'a gagner, sinon, renvoie le numero du joueur gagnant
*/
int aGagnerVerticale(int ttint_plateau[N][N]){
  //DECLARATION DES VARIABLES
  int int_i; //variables qui va parcourir les indice des lignes du tableau
  int int_j; //variables qui va parcourir les indice des colonnes du tableau
  int int_nbr; //le nombre de pion aligné
  int int_tmp; //la valeur dans la case précédente
  int int_valeurReturn; //la valeur à retourner

  //INITIALISATION DES VARIABLES
  //on considère au départ que personne n'a gagner
  int_valeurReturn = -1;

  //parcourir toutes les colonnes
  for(int_j = 0 ; int_j < N ; int_j++){
    //(ré)initialisation du nombre de pion aligné
    int_nbr = 1;
    //(ré)initialisation de la case précédente
    int_tmp = -1;
    //parcourir toutes les lignes
    for(int_i = 0 ; int_i < N ; int_i++){
      //si la case courante à été joué
      if(ttint_plateau[int_i][int_j]!=-1){
        //si un pion est égal au précédent
        if(ttint_plateau[int_i][int_j]==int_tmp){
          //incrémenter le nombre de pion aligné
          int_nbr++;
          //si 4 pions sont alligné, on retourne alors le numéro du joueur gagnant
          if(int_nbr == 4) int_valeurReturn = ttint_plateau[int_i][int_j];
        }
        //sinon = si la case courante n'est pas égale à la précedente
        else{
          //on réinitialise le nombre de pion aligné
          int_nbr = 1;
        }
      }
      //sinon = si la case courante n'a pas été joué
      else{
          //on réinitialise le nombre de pion aligné
          int_nbr = 1;
      }
      //mettre à jour la case précédente
      int_tmp=ttint_plateau[int_i][int_j];
    }
  }
  //si le programmearrive jusque là, alors personne n'a gagné, on renvoie alors -1
  return(int_valeurReturn);
}

/*!
\fn int aGagnerDiagonales ( int ttint_plateau[N][N] )
\author Jalbert Sylvain
\version 0.1 Premier jet
\date 8 novembre 2019
\brief une fonction qui va verifier si 4 pions sont aligné diagonalement
\param ttint_plateau[N][N] tableau de tableau d'entier, représentant la grille de jeu
\return -1 si personne n'a gagner, sinon, renvoie le numero du joueur gagnant
*/
int aGagnerDiagonales(int ttint_plateau[N][N]){
  //DECLARATION DES VARIABLES
  int int_i; //variables qui va parcourir les indice des lignes du tableau
  int int_j; //variables qui va parcourir les indice des colonnes du tableau
  int int_nbrBDSup; //le nombre de pion aligné sur la partie superieure et la diagonale centrale de la diagonale qui a son Bas à Droite
  int int_nbrBDInf; //le nombre de pion aligné sur la partie inferieure à la diagonale centrale de la diagonale qui a son Bas à Droite
  int int_tmpBDSup; //la valeur dans la case précédente de la partie superieure de la diagonale qui a son Bas à Droite
  int int_tmpBDInf; //la valeur dans la case précédente de la partie inferieure de la diagonale qui a son Bas à Droite
  int int_nbrBGSup; //le nombre de pion aligné sur la partie superieure et la diagonale centrale de la diagonale qui a son Bas à Gauche
  int int_nbrBGInf; //le nombre de pion aligné sur la partie inferieure à la diagonale centrale de la diagonale qui a son Bas à Gauche
  int int_tmpBGSup; //la valeur dans la case précédente de la partie superieure de la diagonale qui a son Bas à Gauche
  int int_tmpBGInf; //la valeur dans la case précédente de la partie inferieure de la diagonale qui a son Bas à Gauche
  int int_caseCourante; //la case courante qui sera traité dans le parcour de la grille
  int int_valeurReturn; //la valeur à retourner

  //INITIALISATION DES VARIABLES
  //on considère au départ que personne n'a gagner
  int_valeurReturn = -1;

  //PARCOURIR TOUTES LES DIAGONALES QUI FONT 4 OU PLUS DE LONGUEUR
  //parcourir toutes les lignes de 0 à N-4
  for(int_i = 0 ; int_i < N-3 ; int_i++){
    //(ré)initialisation du nombre de pion aligné dans la partie inferieur et superieure
    // de la diagonale qui a son Bas à Droite
    int_nbrBDSup = 1;
    int_nbrBDInf = 1;
    //(ré)initialisation du nombre de pion aligné dans la partie inferieur et superieure
    // de la diagonale qui a son Bas à Gauche
    int_nbrBGSup = 1;
    int_nbrBGInf = 1;
    //on initialise les valeurs des cases précédentes à -1, qui correspond à une case non joué
    // de la diagonale qui a son Bas à Droite
    int_tmpBDSup = -1;
    int_tmpBDInf = -1;
    //on initialise les valeurs des cases précédentes à -1, qui correspond à une case non joué
    // de la diagonale qui a son Bas à Gauche
    int_tmpBGSup = -1;
    int_tmpBGInf = -1;
    //parcourir toutes les colonnes de int_i à N-1
    for(int_j = int_i ; int_j < N ; int_j++){
      //Partie superieure de la diagonale ↘
      //on défini la case courante
      int_caseCourante = ttint_plateau[N-1-int_j][N-1-int_j+int_i];
      //si la case courante a été joué et si un pion est égal au précédent
      if(int_caseCourante!=-1 && int_caseCourante==int_tmpBDSup){
        //incrémenter le nombre de pion aligné
        int_nbrBDSup++;
        //si 4 pions sont alligné, on retourne alors le numéro du joueur gagnant
        if(int_nbrBDSup == 4) int_valeurReturn = int_caseCourante;
      }
      //sinon = si la case courante n'a pas été joué ou que le un pion n'est pas égal au précédent
      else{
          //on réinitialise le nombre de pion aligné dans la partie superieure
          int_nbrBDSup = 1;
      }
      //mettre à jour la future case précédente de la partie superieure
      int_tmpBDSup=int_caseCourante;

      //Partie inferieure de la diagonale ↘
      //ne doit pas traiter la diagonale centrale
      if(int_i!=0){
        //on défini la case courante
        int_caseCourante = ttint_plateau[int_j][int_j-int_i];
        //si la case courante a été joué et si un pion est égal au précédent
        if(int_caseCourante!=-1 && int_caseCourante==int_tmpBDInf){
          //incrémenter le nombre de pion aligné
          int_nbrBDInf++;
          //si 4 pions sont alligné, on retourne alors le numéro du joueur gagnant
          if(int_nbrBDInf == 4) int_valeurReturn = int_caseCourante;
        }
        //sinon = si la case courante n'a pas été joué ou que le un pion n'est pas égal au précédent
        else{
            //on réinitialise le nombre de pion aligné dans la partie inferieure
            int_nbrBDInf = 1;
        }
        //mettre à jour la future case précédente de la partie inferieure
        int_tmpBDInf=int_caseCourante;
      }

      //Partie superieure de la diagonale ↗
      //on défini la case courante
      int_caseCourante = ttint_plateau[N-1-int_j][int_j-int_i];
      //si la case courante a été joué et si un pion est égal au précédent
      if(int_caseCourante!=-1 && int_caseCourante==int_tmpBGSup){
        //incrémenter le nombre de pion aligné
        int_nbrBGSup++;
        //si 4 pions sont alligné, on retourne alors le numéro du joueur gagnant
        if(int_nbrBGSup == 4) int_valeurReturn = int_caseCourante;
      }
      //sinon = si la case courante n'a pas été joué ou que le un pion n'est pas égal au précédent
      else{
          //on réinitialise le nombre de pion aligné dans la partie superieure
          int_nbrBGSup = 1;
      }
      //mettre à jour la future case précédente de la partie superieure
      int_tmpBGSup=int_caseCourante;

      //Partie inferieure de la diagonale ↗
      //ne doit pas traiter la diagonale centrale
      if(int_i!=0){
        //on défini la case courante
        int_caseCourante = ttint_plateau[int_j][N-1-int_j+int_i];
        //si la case courante a été joué et si un pion est égal au précédent
        if(int_caseCourante!=-1 && int_caseCourante==int_tmpBGInf){
          //incrémenter le nombre de pion aligné
          int_nbrBGInf++;
          //si 4 pions sont alligné, on retourne alors le numéro du joueur gagnant
          if(int_nbrBGInf == 4) int_valeurReturn = int_caseCourante;
        }
        //sinon = si la case courante n'a pas été joué ou que le un pion n'est pas égal au précédent
        else{
            //on réinitialise le nombre de pion aligné dans la partie inferieure
            int_nbrBGInf = 1;
        }
        //mettre à jour la future case précédente de la partie inferieure
        int_tmpBGInf=int_caseCourante;
      }
    }
  }
  //retourner le résultat
  return(int_valeurReturn);
}

/*!
\fn int aGagner ( int ttint_plateau[N][N] )
\author Jalbert Sylvain
\version 0.1 Premier jet
\date 8 novembre 2019
\brief une fonction qui va verifier si il y a un gagnant ou pas
\param ttint_plateau[N][N] tableau de tableau d'entier, représentant la grille de jeu
\return 0 si le match est nul, -1 si personne n'a gagner, sinon, renvoie le numero du joueur gagnant
*/
int aGagner(int ttint_plateau[N][N]){
  //DECLARATION DES VARIABLES
  int int_res; //le resultat des differents test pour savoir si un joueur a gagner
  int int_col; //colonne courante dans la parcours du tableau
  int int_estNul; //1 si le match est nul, 0 sinon
  int int_valeurReturn; //la valeur à retourner

  //INITIALISATION DES VARIABLES
  //le parcours du tableau commence à la colonne 0
  int_col = 0;
  //on considère au départ que le match est nul
  int_estNul = 1;
  //on considère au départ que personne n'a gagner
  int_valeurReturn = -1;

  //VERIFIER SI IL Y A UN GAGNANT
  //verification si il y a un gagnant sur les pions aligné horizontalement
  int_res = aGagnerHorizontale(ttint_plateau);
  //si il y a un gagnant, alors on retourne son numero
  if(int_res != -1) int_valeurReturn = int_res;
  //sinon on continue les vérification
  else{
    //verification si il y a un gagnant sur les pions aligné verticalement
    int_res = aGagnerVerticale(ttint_plateau);
    //si il y a un gagnant, alors on retourne son numero
    if(int_res != -1) int_valeurReturn = int_res;
    //sinon on continue les vérification
    else{
      //verification si il y a un gagnant sur les diagonales qui partent d'en bas à guauche vers en haut à droite
      int_res = aGagnerDiagonales(ttint_plateau);
      //si il y a un gagnant, alors on retourne son numero
      if(int_res != -1) int_valeurReturn = int_res;
      //sinon on continue les vérification
      else{
        //verification si le match est nul
        //parcours de la derniere ligne pour savoir si le match est nul
        //on s'arrete si on voit que le match n'est pas nul
        while(int_col < N && int_estNul == 1){
          //si la case courante est égale à -1, celle ci n'a pas été joué
          //alors le match n'est pas nul
          if(ttint_plateau[0][int_col] == -1) int_estNul = 0;
          //on passe à la case suivante, donc on incrémente la colonne
          int_col++;
        }

        //si le match est nul on retourne 0
        if(int_estNul == 1) int_valeurReturn = 0;
      }
    }
  }
  //on retourne la valeur de int_valeurReturn
  return(int_valeurReturn);
}
