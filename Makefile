#File Makefile
#Autor Sylvain Jalbert
#Date 14 novembre 2019
#Resum le fichier de configuration premetant d'executer tout le code grace à la commande "make"

CC = gcc -Wall
PROG = puissance4

all : $(PROG)

$(PROG) : jeu.o buffer.o aGagner.o main.o
	gcc $^ -o $@

main.o : main.c
	$(CC) -c $^ -o $@

jeu.o : jeu.c jeu.h
	$(CC) -c jeu.c -o $@

buffer.o : buffer.c buffer.h
	$(CC) -c buffer.c -o $@

aGagner.o : aGagner.c aGagner.h
	$(CC) -c aGagner.c -o $@

.PHONY : clean

clean :
	rm -i jeu.o buffer.o aGagner.o main.o
