/*!
\file buffer.h
\autor Jalbert Sylvain
\version 1
\date 14 novembre 2019
\brief le fichier d'entête du fichier buffer.c qui contient la déclaration de la fonction relative au buffer
*/

#ifndef __BUFFER_H_
#define __BUFFER_H_

//on utilise la librairie utile pour les interactions avec l'utilisateur
#include <stdio.h>

/*!
\fn void viderBuffer ( void )
\author Jalbert Sylvain
\version 0.1 Premier jet
\date 11 novembre 2019
\brief une procedure qui va vider le buffer
*/
void viderBuffer(void);

#endif
