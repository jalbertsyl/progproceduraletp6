/*!
\file main.c
\autor Jalbert Sylvain
\version 1
\date 8 novembre 2019
\brief un programme pour jouer au jeu du puissance 4
*/

#include "aGagner.h"
#include "jeu.h"

/*!
\fn int main ( int argc, char∗∗ argv )
\author Jalbert Sylvain
\version 0.1 Premier jet
\date 8 novembre 2019
\brief la fonction principale qui gère le jeu
\param argc nombre d’arguments en entree
\param argv valeur des arguments en entree
\return 0 si tout c'est bien passé
*/
int main (int argc, char** argv){
  //DECLARATION DES VARIABLES
  int ttint_grille[N][N]; //la grille de jeu

  //INITIALISER LA GRILLE DE JEU
  init(ttint_grille);

  //AFFICHAGE TABLEAU
  affichage(ttint_grille);

  //Lancer la partie
  tourDeJeu(ttint_grille);

  /* Fin du programme, Il se termine normalement, et donc retourne 0 */
  return 0;
}
