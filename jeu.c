/*!
\file jeu.c
\autor Jalbert Sylvain
\version 1
\date 14 novembre 2019
\brief le fichier qui contient toutes les fonction relatif au comportement du jeu
*/

#include "jeu.h"

/*!
\fn void init ( int ttint_plateau[N][N] )
\author Jalbert Sylvain
\version 0.1 Premier jet
\date 8 novembre 2019
\brief une procedure qui initialise le tableau de tableau en mettant des -1 dans ttes les cases
\param ttint_plateau[N][N] tableau de tableau d'entier, représentant la grille de jeu
*/
void init(int ttint_plateau[N][N]){
  //DECLARATION DES VARIABLES
  int int_i; //variables qui va parcourir les indice des lignes du tableau
  int int_j; //variables qui va parcourir les indice des colonnes du tableau

  //INITIALISATION DU TABLEAU
  for(int_i = 0 ; int_i < N ; int_i++){
    for(int_j = 0 ; int_j < N ; int_j++){
      ttint_plateau[int_i][int_j] = -1;
    }
  }
}

/*!
\fn void affichage ( int ttint_plateau[N][N] )
\author Jalbert Sylvain
\version 0.1 Premier jet
\date 8 novembre 2019
\brief une procedure qui affiche la grille passé en paramettre
\param ttint_plateau[N][N] tableau de tableau d'entier, représentant la grille de jeu
*/
void affichage(int ttint_plateau[N][N]){
  //DECLARATION DES VARIABLES
  int int_i; //variables qui va parcourir les indice des lignes du tableau
  int int_j; //variables qui va parcourir les indice des colonnes du tableau

  //AFFICHAGE DE LA GRILLE
  //afficher le décalage
  printf("\n  ");
  //affichage des indices colonnes
  for(int_i = 0 ; int_i < N ; int_i++){
    printf("  %d ", int_i);
  }
  //affichage fin de ligne
  printf("\n");
  //parcour de chaque lignes
  for(int_i = 0 ; int_i < N ; int_i++){
    //affichage du séparateur
    //affichage du décalage du séparateur
    printf("  ");
    //affichage du séparateur de longeur N
    for(int_j = 0 ; int_j < N ; int_j++){
      printf("+---");
    }
    //affichage du dernier caractère du séparateur + retour ligne + numéro de la ligne courante
    printf("+\n%d ",int_i);
    //parcour de chaque case de la ligne
    for(int_j = 0 ; int_j < N ; int_j++){
      //afficher un séparateur et le contenu correspondant à la valeur de la case
      printf("| %s ", (ttint_plateau[int_i][int_j]==-1 ? " " : (ttint_plateau[int_i][int_j]==1 ? "X" : "O")));
    }
    //afficher le dernier separateur de colonne + fin de la ligne
    printf("|\n");
  }
  //afficher le dernier separateur de ligne
  //affichage du décalage du séparateur
  printf("  ");
  //affichage du séparateur de longeur N
  for(int_j = 0 ; int_j < N ; int_j++){
    printf("+---");
  }
  //affichage du dernier caractère du séparateur + retour ligne + numéro de la ligne courante
  printf("+\n%d ",int_i);
}

/*!
\fn int jouer ( int ttint_plateau[N][N], int int_joueur, int int_x )
\author Jalbert Sylvain
\version 0.1 Premier jet
\date 8 novembre 2019
\brief une fonction qui va permetre à un joueur de jouer
\param ttint_plateau[N][N] tableau de tableau d'entier, représentant la grille de jeu
\param int_joueur le numero de joueur qui joue
\param int_x colonne que le joueur joue
\return 1 si tout c'est bien passé, 0 sinon
*/
int jouer(int ttint_plateau[N][N], int int_joueur, int int_x){
  //DECLARATION DES VARIABLES
  int int_res; //resultat à retourner. Est égal à 1 si tout c'est bien passé, 0 sinon
  int int_i; //compteur pour parcourir les lignes de la grille

  //INITIALISATION DES VARIABLES
  //On considère qu'une erreur est survenu
  int_res = 0;
  //on va commencer le parcours depuis la premiere case du tableau
  int_i = N-1;

  //JOUER SI LA COLONNE N'EST PAS PLEINE
  //si la premiere ligne de la colonne int_x est égale à -1 cela signifie que la colonne n'est pas pleine
  if(ttint_plateau[0][int_x]==-1){
    //tant que le resultat est égale à 0 cela signifie que on n'a pas encore placer le pion du joueur
    while(int_res==0){
      //si la case courante est égale à -1 c'st que l'on a trouvé la premiere case vide
      if(ttint_plateau[int_i][int_x]==-1){
        //on place le pion
        ttint_plateau[int_i][int_x]=int_joueur;
        //le tour c'est bien passé, le pion à été placé
        int_res=1;
      }
      int_i--;
    }
  }
  //Si la colonne est pleine, afficher un message d'erreur
  else printf("La colonne est pleine !\n");

  /* Fin du programme, int_res est retourné */
  return int_res;
}

/*!
\fn void tourDeJeu ( int ttint_plateau[N][N] )
\author Jalbert Sylvain
\version 0.1 Premier jet
\date 11 novembre 2019
\brief une procedure qui va faire jouer les joueurs chaqu'un leurs tours jusqu'a la fin de la partie et afficher le gagnant
\param ttint_plateau[N][N] tableau de tableau d'entier, représentant la grille de jeu
*/
void tourDeJeu(int ttint_plateau[N][N]){
  //DECLARATION DES VARIABLES
  int int_numJoueur; //le numéro du joueur qui sera entrain de jouer
  int int_etat; //l'état en cour de la partie (qui sera le resultat de la fonction aGagner)
  int int_col; //colonne que le joueur va jouer

  //INITIALISATION DES VARIABLES
  int_numJoueur = 1; //le premier joueur commencera la partie
  int_etat = -1; //au début de la partie, il n'y a pas de gagnant

  //INITIALISATION DE L'ECRAN
  //Effacer l'ecran
  system("clear");

  //DEROULEMENT DU JEU
  //tant que la partie n'est pas terminer :
  while (int_etat==-1) {
    //affichage de la grille avant de jouer
    affichage(ttint_plateau);
    //demander au joueur de jouer
    printf("Joueur %d -> quelle colonne ? ", int_numJoueur);
    //lecture de la colonne à jouer
    while (!scanf("%d", &int_col)) {
      viderBuffer();
      printf("Saisie incorrect ! Veuillez saisir un numéro de colonne :");
    }
    //Effacer l'ecran
    system("clear");
    //le tour s'effectue que si la colone saisie existe
    if(int_col >= 0 && int_col < N){
      //jouer la partie
      //si ça c'est bien passé alors on actualise l'état de la partie et on passe au joueur suivant
      if(jouer(ttint_plateau,int_numJoueur,int_col)){
        //actualisation de l'état de la partie
        int_etat=aGagner(ttint_plateau);
        //changement de joueur pour le tour suivant
        int_numJoueur = (int_numJoueur==1?2:1);
      }
    }
    else{
      //dire que la valeur de colonne n'est pas bonne
      printf("Saisie incorrect ! La colonne saisie n'existe pas !\n");
    }
  }

  //AFFICHAGE DE FIN DE PARTIE
  //on montre que la partie est terminée
  printf("\n\t\t|\\   /|\n\t\t \\|_|/\n\t\t /. .\\\n\t\t=\\_Y_/=\n\tPARTIE TERMINEE\n");
  //affichage de la grille finale
  affichage(ttint_plateau);

  //AFFICHAGE DU GAGNANT
  //message indiquant à l'utilisateur que le résultat se trouve ici
  printf("Résultat -> ");
  //Si le match est nul on affiche que le match est nul
  if(int_etat == 0) printf("Match nul !\n");
  //Sinon on affiche le gagnant dont le numero est dans la variables int_etat
  else printf("Joueur %d à gagner !\n", int_etat);
}
