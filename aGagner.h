/*!
\file aGagner.h
\autor Jalbert Sylvain
\version 1
\date 14 novembre 2019
\brief le fichier d'entête du fichier aGagner.c qui contient les déclaration des fonctions
*/

#ifndef __AGAGNER_H_
#define __AGAGNER_H_

#define N 7
//on utilise la librairie utile pour les interactions avec l'utilisateur
#include <stdio.h>

/*!
\fn int aGagnerHorizontale ( int ttint_plateau[N][N] )
\author Jalbert Sylvain
\version 0.1 Premier jet
\date 8 novembre 2019
\brief une fonction qui va verifier si 4 pions sont aligné horizontalement
\param ttint_plateau[N][N] tableau de tableau d'entier, représentant la grille de jeu
\return -1 si personne n'a gagner, sinon, renvoie le numero du joueur gagnant
*/
int aGagnerHorizontale(int ttint_plateau[N][N]);

/*!
\fn int aGagnerVerticale ( int ttint_plateau[N][N] )
\author Jalbert Sylvain
\version 0.1 Premier jet
\date 8 novembre 2019
\brief une fonction qui va verifier si 4 pions sont aligné verticalement
\param ttint_plateau[N][N] tableau de tableau d'entier, représentant la grille de jeu
\return -1 si personne n'a gagner, sinon, renvoie le numero du joueur gagnant
*/
int aGagnerVerticale(int ttint_plateau[N][N]);

/*!
\fn int aGagnerDiagonales ( int ttint_plateau[N][N] )
\author Jalbert Sylvain
\version 0.2
\date 10 novembre 2019
\brief une fonction qui va verifier si 4 pions sont aligné diagonalement
\param ttint_plateau[N][N] tableau de tableau d'entier, représentant la grille de jeu
\return -1 si personne n'a gagner, sinon, renvoie le numero du joueur gagnant
*/
int aGagnerDiagonales(int ttint_plateau[N][N]);

/*!
\fn int aGagner ( int ttint_plateau[N][N] )
\author Jalbert Sylvain
\version 0.2
\date 10 novembre 2019
\brief une fonction qui va verifier si il y a un gagnant ou pas dans la grille donné en paramettre
\param ttint_plateau[N][N] tableau de tableau d'entier, représentant la grille de jeu
\return 0 si le match est nul, -1 si personne n'a gagner, sinon, renvoie le numero du joueur gagnant
*/
int aGagner(int ttint_plateau[N][N]);

#endif
