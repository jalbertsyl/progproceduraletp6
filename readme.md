# Readme.md
Ce fichier readme.md contient les instructions pour compiler et executer le programme du TP. Ainsi que les instructions pour générer la documentation.

## Documentation
La documentation est faite avec doxygen. Pour l'observée, executer la comande suivante :

    doxygen Doxyfile

Puis visualiser dans votre navigateur le fichier ./html/index.html

## Programme
### Compiler
Pour compiler, il faut saisir dans le terminal linux, à la racine du projet, la commande suivante :

    make

Grâce au fichier de configuration "Makefile", tous les fichier du programme se sont compilé. Cependant, des fichiers temporaires ont été créer ! Pour les effacer, executer la commande suivante :

    make clear

### Executer
Pour executer, il suffi de saisir dans le terminal linux, à la racine du projet, la commande suivante :

    ./puissance4
