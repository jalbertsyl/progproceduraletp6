/*!
\file buffer.c
\autor Jalbert Sylvain
\version 1
\date 14 novembre 2019
\brief le fichier qui contient toutes la fonction pour vider le buffer
*/

#include "buffer.h"

/*!
\fn void viderBuffer ( void )
\author Jalbert Sylvain
\version 0.1 Premier jet
\date 11 novembre 2019
\brief une procedure qui va vider le buffer
*/
void viderBuffer(void){
  //DECLARATION DES VARIABLES
  char char_saisie; //le caracère courant dans le buffer

  //INITIALISATION DES VARIABLES
  scanf("%c", &char_saisie);

  //VIDER LE BUFFER
  //tant que le caractère courant est different de \n
  while (char_saisie!='\n') {
    //vider du buffer, le caractère suivant
    scanf("%c", &char_saisie);
  }
}
